# -*- coding: utf-8 -*-

"""
***************************************************************************
*   ZONE LA PLUS PROPICE                                                  *
*   Trouver la zone la plus propice selon les criteres :                  *
*   - les gares SNCF à moins d'un km                                      *
*   - les stations de metro à moins de 300m                               *
*   - les espaces verts à moins de 200m                                   *
*   - les piscines à moins de 500m                                        *
***************************************************************************
"""

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingException,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFeatureSink,
                       QgsProcessingParameterVectorDestination,
                       QgsProcessingParameterDistance)
from qgis import processing


class EldoradoAlgorithm(QgsProcessingAlgorithm):
    """
    This is an example algorithm that takes a vector layer and
    creates a new identical one.

    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.

    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    # Les parametres d'entree : 4 couches + 4 distances
    INPUT1 = 'INPUT1'
    DIST1 = 'DIST1'

    INPUT2 = 'INPUT2'
    DIST2 = 'DIST2'

    INPUT3 = 'INPUT3'
    DIST3 = 'DIST3'

    INPUT4 = 'INPUT4'
    DIST4 = 'DIST4'

    OUTPUT = 'OUTPUT'


    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return EldoradoAlgorithm()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'eldorado'

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr('The Eldorado')

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr('Eldorado')

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'eldorado'

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        return self.tr("Where is the Eldorado ?")

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        # Couches source
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT1,
                self.tr('Input layer'),
                [QgsProcessing.TypeVectorAnyGeometry]
            )
        )
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT2,
                self.tr('Input layer'),
                [QgsProcessing.TypeVectorAnyGeometry]
            )
        )
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT3,
                self.tr('Input layer'),
                [QgsProcessing.TypeVectorAnyGeometry]
            )
        )
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT4,
                self.tr('Input layer'),
                [QgsProcessing.TypeVectorAnyGeometry]
            )
        )

        # Distances des tampons
        self.addParameter(
            QgsProcessingParameterDistance(
                'DIST1',
                self.tr('Taille du tampon (m)'),
                defaultValue = 1000.0,
                # Make distance units match the INPUT layer units:
                parentParameterName='INPUT1'
            )
        )
        self.addParameter(
            QgsProcessingParameterDistance(
                'DIST2',
                self.tr('Taille du tampon (m)'),
                defaultValue = 1000.0,
                # Make distance units match the INPUT layer units:
                parentParameterName='INPUT2'
            )
        )
        self.addParameter(
            QgsProcessingParameterDistance(
                'DIST3',
                self.tr('Taille du tampon (m)'),
                defaultValue = 1000.0,
                # Make distance units match the INPUT layer units:
                parentParameterName='INPUT3'
            )
        )
        self.addParameter(
            QgsProcessingParameterDistance(
                'DIST4',
                self.tr('Taille du tampon (m)'),
                defaultValue = 1000.0,
                # Make distance units match the INPUT layer units:
                parentParameterName='INPUT4'
            )
        )
        # Récupération de la destination
        self.addParameter(
            QgsProcessingParameterVectorDestination(
                self.OUTPUT,
                self.tr('Sortie')
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """
        # Recuperation des parametres
        outputFile = self.parameterAsOutputLayer(parameters, self.OUTPUT, context)
        dist1 = self.parameterAsDouble(parameters, 'DIST1', context)
        dist2 = self.parameterAsDouble(parameters, 'DIST2', context)
        dist3 = self.parameterAsDouble(parameters, 'DIST3', context)
        dist4 = self.parameterAsDouble(parameters, 'DIST4', context)
        
        chemin = "/Users/theeba/Documents/CARTHAGEO/pyQGIS/Lyon/"

        # Creation des 4 tampons
        tampon1 = processing.run(
            "native:buffer",{
                'INPUT': parameters['INPUT1'],
                'DISTANCE' : dist1,
                'SEGMENTS' :5,
                'END CAP STYLE' : 0,
                'JOIN STYLE': 0,
                'MITER LIMIT' :2,
                'DISSOLVE' : False,
                'OUTPUT' : chemin + "tampon1"},
            context=context,
            feedback=feedback)

        tampon2 = processing.run(
            "native:buffer",{
                'INPUT': parameters['INPUT2'],
                'DISTANCE' : dist2,
                'SEGMENTS' :5,
                'END CAP STYLE' : 0,
                'JOIN STYLE': 0,
                'MITER LIMIT' :2,
                'DISSOLVE' : False,
                'OUTPUT' : chemin + "tampon2"},
            context=context,
            feedback=feedback)

        tampon3 = processing.run(
            "native:buffer",{
                'INPUT': parameters['INPUT3'],
                'DISTANCE' : dist3,
                'SEGMENTS' :5,
                'END CAP STYLE' : 0,
                'JOIN STYLE': 0,
                'MITER LIMIT' :2,
                'DISSOLVE' : False,
                'OUTPUT' : chemin + "tampon3"},
            context=context,
            feedback=feedback)

        tampon4 = processing.run(
            "native:buffer",{
                'INPUT': parameters['INPUT4'],
                'DISTANCE' : dist4,
                'SEGMENTS' :5,
                'END CAP STYLE' : 0,
                'JOIN STYLE': 0,
                'MITER LIMIT' :2,
                'DISSOLVE' : False,
                'OUTPUT' : chemin + "tampon4"},
            context=context,
            feedback=feedback)

        # Intersection des tampons
        intersection1 = processing.run(
            "native:intersection",
            {'INPUT':tampon1['OUTPUT'],
            'OVERLAY':tampon2['OUTPUT'],
            'INPUT_FIELDS':[],
            'OVERLAY_FIELDS':[],
            'OUTPUT': chemin + 'inter1'})['OUTPUT']

        intersection2 = processing.run(
            "native:intersection",
            {'INPUT':intersection1,
            'OVERLAY': tampon3['OUTPUT'],
            'INPUT_FIELDS':[],
            'OVERLAY_FIELDS':[],
            'OUTPUT': chemin + 'inter2'})['OUTPUT']

        intersection = processing.runAndLoadResults(
            "native:intersection",
            {'INPUT':intersection2,
            'OVERLAY': tampon4['OUTPUT'],
            'INPUT_FIELDS':[],
            'OVERLAY_FIELDS':[],
            'OUTPUT': outputFile})['OUTPUT']

        # Retourn le résultat
        return {self.OUTPUT: intersection}

        ###############################################################

        # If source was not found, throw an exception to indicate that the algorithm
        # encountered a fatal error. The exception text can be any string, but in this
        # case we use the pre-built invalidSourceError method to return a standard
        # helper text for when a source cannot be evaluated
        if source is None:
            raise QgsProcessingException(self.invalidSourceError(parameters, self.INPUT))

        (sink, dest_id) = self.parameterAsSink(
            parameters,
            self.OUTPUT,
            context,
            source.fields(),
            source.wkbType(),
            source.sourceCrs()
        )

        # Send some information to the user
        feedback.pushInfo('CRS is {}'.format(source.sourceCrs().authid()))

        # If sink was not created, throw an exception to indicate that the algorithm
        # encountered a fatal error. The exception text can be any string, but in this
        # case we use the pre-built invalidSinkError method to return a standard
        # helper text for when a sink cannot be evaluated
        if sink is None:
            raise QgsProcessingException(self.invalidSinkError(parameters, self.OUTPUT))

        # Compute the number of steps to display within the progress bar and
        # get features from source
        total = 100.0 / source.featureCount() if source.featureCount() else 0
        features = source.getFeatures()

        for current, feature in enumerate(features):
            # Stop the algorithm if cancel button has been clicked
            if feedback.isCanceled():
                break

            # Add a feature in the sink
            sink.addFeature(feature, QgsFeatureSink.FastInsert)

            # Update the progress bar
            feedback.setProgress(int(current * total))

        # To run another Processing algorithm as part of this algorithm, you can use
        # processing.run(...). Make sure you pass the current context and feedback
        # to processing.run to ensure that all temporary layer outputs are available
        # to the executed algorithm, and that the executed algorithm can send feedback
        # reports to the user (and correctly handle cancellation and progress reports!)
        if False:
            buffered_layer = processing.run("native:buffer", {
                'INPUT': dest_id,
                'DISTANCE': 1.5,
                'SEGMENTS': 5,
                'END_CAP_STYLE': 0,
                'JOIN_STYLE': 0,
                'MITER_LIMIT': 2,
                'DISSOLVE': False,
                'OUTPUT': 'memory:'
            }, context=context, feedback=feedback)['OUTPUT']

        # Return the results of the algorithm. In this case our only result is
        # the feature sink which contains the processed features, but some
        # algorithms may return multiple feature sinks, calculated numeric
        # statistics, etc. These should all be included in the returned
        # dictionary, with keys matching the feature corresponding parameter
        # or output names.
        return {self.OUTPUT: dest_id}
