# -*- coding: utf-8 -*-

"""
*****************************************************************************
*                                                                           *
*   Ce script permet de localiser les entités d'un couche de superposition  *
*   dans une distance autour d'une couche source                            *
*****************************************************************************
"""

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingException,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFeatureSink,
                       QgsProcessingParameterVectorDestination,
                       QgsProcessingParameterDistance)
from qgis import processing


class ProximiteAlgorithm(QgsProcessingAlgorithm):
    """
    This is an example algorithm that takes a vector layer and
    creates a new identical one.

    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.

    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.
    """
    # Constantes :
    SOURCE = 'SOURCE' # couche qui sera intersectee
    SUPERPOSITION = 'SUPERPOSITION' # couche pour creer le buffer
    BUFFER_OUTPUT = 'BUFFER_OUTPUT' # stockage buffer
    OUTPUT = 'OUTPUT' # couche en sortie

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return ProximiteAlgorithm()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'proximite'

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("Proximite")

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("Proximite")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'proximitescripts'

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        return self.tr("The object you're looking for is somewhere close...")

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """
        # Donnees source
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.SOURCE,
                self.tr('Couche source'),
                [QgsProcessing.TypeVectorAnyGeometry]
            )
        )
        # Donnees superposees
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.SUPERPOSITION,
                self.tr('Couche en superposition')
                [QgsProcessing.TypeVectorAnyGeometry]
            )
        )
        # Distance du tampon
        self.addParameter(
            QgsProcessingParameterDistance(
                'BUFFERDIST',
                self.tr('Taille du tampon (m)'),
                defaultValue = 1000.0,
                # Make distance units match the INPUT layer units:
                parentParameterName='SUPERPOSITION'
            )
        )
        # Récupération de la destination
        self.addParameter(
            QgsProcessingParameterVectorDestination(
                self.OUTPUT,
                self.tr('Sortie')
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """

        # Retrieve the feature source and sink. The 'dest_id' variable is used
        # to uniquely identify the feature sink, and must be included in the
        # dictionary returned by the processAlgorithm function.
        # source = self.parameterAsSource(parameters,self.INPUT,context)

        # on recupere les parametres
        outputFile = self.parameterAsOutputLayer(parameters, self.OUTPUT, context)
        bufferdist = self.parameterAsDouble(parameters, 'BUFFERDIST', context)

        # Creation d'une zone tampon
        tampon = processing.run(
            "native:buffer",{
                'INPUT': parameters['SUPERPOSITION'],
                'DISTANCE' : bufferdist,
                'SEGMENTS' :5,
                'END CAP STYLE' : 0,
                'JOIN STYLE': 0,
                'MITER LIMIT' :2,
                'DISSOLVE' : False,
                'OUTPUT' : "/Users/theeba/Documents/CARTHAGEO/pyQGIS/Scripts/buffer_output" # Si je n'enregistre pas cette couche, le script ne marche pas...
                }, context=context, feedback=feedback)

        # Intersection du tampon et de la couche source
        intersection = processing.runAndLoadResults(
            "native:intersection",
            {'INPUT':parameters['SOURCE'],
            'OVERLAY': tampon['OUTPUT'],
            'INPUT_FIELDS':[],
            'OVERLAY_FIELDS':[],
            'OUTPUT': outputFile})['OUTPUT']

        # Retourn le résultat de l'intersection du tampon et de la couche source
        return {self.OUTPUT: intersection}

        ################################################

        # If source was not found, throw an exception to indicate that the algorithm
        # encountered a fatal error. The exception text can be any string, but in this
        # case we use the pre-built invalidSourceError method to return a standard
        # helper text for when a source cannot be evaluated
        if source is None:
            raise QgsProcessingException(self.invalidSourceError(parameters, self.INPUT))

        (sink, dest_id) = self.parameterAsSink(
            parameters,
            self.OUTPUT,
            context,
            source.fields(),
            source.wkbType(),
            source.sourceCrs()
        )

        # Send some information to the user
        feedback.pushInfo('CRS is {}'.format(source.sourceCrs().authid()))

        # If sink was not created, throw an exception to indicate that the algorithm
        # encountered a fatal error. The exception text can be any string, but in this
        # case we use the pre-built invalidSinkError method to return a standard
        # helper text for when a sink cannot be evaluated
        if sink is None:
            raise QgsProcessingException(self.invalidSinkError(parameters, self.OUTPUT))

        # Compute the number of steps to display within the progress bar and
        # get features from source
        total = 100.0 / source.featureCount() if source.featureCount() else 0
        features = source.getFeatures()

        for current, feature in enumerate(features):
            # Stop the algorithm if cancel button has been clicked
            if feedback.isCanceled():
                break

            # Add a feature in the sink
            sink.addFeature(feature, QgsFeatureSink.FastInsert)

            # Update the progress bar
            feedback.setProgress(int(current * total))

        # To run another Processing algorithm as part of this algorithm, you can use
        # processing.run(...). Make sure you pass the current context and feedback
        # to processing.run to ensure that all temporary layer outputs are available
        # to the executed algorithm, and that the executed algorithm can send feedback
        # reports to the user (and correctly handle cancellation and progress reports!)
        if False:
            buffered_layer = processing.run("native:buffer", {
                'INPUT': dest_id,
                'DISTANCE': 1.5,
                'SEGMENTS': 5,
                'END_CAP_STYLE': 0,
                'JOIN_STYLE': 0,
                'MITER_LIMIT': 2,
                'DISSOLVE': False,
                'OUTPUT': 'memory:'
            }, context=context, feedback=feedback)['OUTPUT']

        # Return the results of the algorithm. In this case our only result is
        # the feature sink which contains the processed features, but some
        # algorithms may return multiple feature sinks, calculated numeric
        # statistics, etc. These should all be included in the returned
        # dictionary, with keys matching the feature corresponding parameter
        # or output names.
        return {self.OUTPUT: dest_id}
